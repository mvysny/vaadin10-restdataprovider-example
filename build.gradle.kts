import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.72"
    id("org.gretty") version "3.0.3"
    id("com.vaadin") version "0.7.0" apply(false)
}

defaultTasks("clean", "build")


allprojects {
    group = "com.example.vok"
    version = "1.0-SNAPSHOT"

    repositories {
        jcenter()
    }
}

subprojects {

    apply { plugin("kotlin") }
    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    tasks.withType<Test> {
        useJUnitPlatform()
        testLogging {
            // to see the exceptions of failed tests in CI console.
            exceptionFormat = TestExceptionFormat.FULL
        }
    }
}
