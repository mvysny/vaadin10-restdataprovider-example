package com.example.vok

import com.github.mvysny.karibudsl.v10.*
import com.github.mvysny.vokdataloader.DataLoader
import com.github.mvysny.vokdataloader.limitChunkSize
import com.vaadin.flow.component.dependency.HtmlImport
import com.vaadin.flow.component.page.BodySize
import com.vaadin.flow.component.page.Viewport
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.router.Route
import com.vaadin.flow.theme.Theme
import com.vaadin.flow.theme.lumo.Lumo
import eu.vaadinonkotlin.restclient.CrudClient
import eu.vaadinonkotlin.vaadin10.*
import eu.vaadinonkotlin.vaadin10.vokdb.asDataProvider
import java.time.Instant

@BodySize(width = "100vw", height = "100vh")
@HtmlImport("frontend://styles.html")
@Route("")
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
@Theme(Lumo::class)
class MainView : KComposite() {

    private val root = ui {
        verticalLayout {
            setSizeFull()
            val crud: DataLoader<Article> = CrudClient("http://localhost:8080/rest/articles/", Article::class.java)
                    .limitChunkSize(100)
            grid<Article>(crud.asDataProvider()) {
                setSizeFull()
                val filterBar: VokFilterBar<Article> = appendHeaderRow().asFilterBar(this)

                addColumnFor(Article::id)
                addColumnFor(Article::title) {
                    filterBar.forField(TextField(), this).ilike()
                }
                addColumnFor(Article::created) {
                    filterBar.forField(DateRangePopup(), this).inRange(Instant::class)
                }
                addColumnFor(Article::score) {
                    filterBar.forField(NumberRangePopup(), this).inRange()
                }
            }
        }
    }
}
