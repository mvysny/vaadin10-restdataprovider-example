plugins {
    war
    id("org.gretty")
    id("com.vaadin")
}

gretty {
    contextPath = "/"
    servletContainer = "jetty9.4"
}

dependencies {
    compile("eu.vaadinonkotlin:vok-framework-v10-vokdb:${properties["vok_version"]}")
    // Vaadin 14
    compile("com.vaadin:vaadin-core:${properties["vaadin10_version"]}") {
        // Webjars are only needed when running in Vaadin 13 compatibility mode
        listOf("com.vaadin.webjar", "org.webjars.bowergithub.insites",
                "org.webjars.bowergithub.polymer", "org.webjars.bowergithub.polymerelements",
                "org.webjars.bowergithub.vaadin", "org.webjars.bowergithub.webcomponents")
                .forEach { exclude(group = it) }
    }
    providedCompile("javax.servlet:javax.servlet-api:3.1.0")

    // logging
    // currently we are logging through the SLF4J API to LogBack. See logback.xml file for the logger configuration
    compile("ch.qos.logback:logback-classic:1.2.3")
    compile("org.slf4j:slf4j-api:1.7.30")
    // this will configure Vaadin to log to SLF4J
    compile("org.slf4j:jul-to-slf4j:1.7.30")

    // db
    compile("org.flywaydb:flyway-core:6.2.4")
    compile("com.h2database:h2:1.4.200")
    compile("com.zaxxer:HikariCP:3.4.2")

    // REST
    compile("eu.vaadinonkotlin:vok-rest:${properties["vok_version"]}")
    compile("eu.vaadinonkotlin:vok-rest-client:${properties["vok_version"]}")

    // Kotlin
    compile(kotlin("stdlib-jdk8"))

    // test
    testCompile("com.github.mvysny.dynatest:dynatest-engine:0.16")
    testCompile("com.github.mvysny.kaributesting:karibu-testing-v10:1.1.25")
    testCompile("org.eclipse.jetty.websocket:websocket-server:9.4.12.v20180830")
}

vaadin {
    pnpmEnable = true
}