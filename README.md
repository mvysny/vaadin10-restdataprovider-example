[![Powered By Vaadin on Kotlin](http://vaadinonkotlin.eu/iconography/vok_badge.svg)](http://vaadinonkotlin.eu)
[![pipeline status](https://gitlab.com/mvysny/vaadin10-restdataprovider-example/badges/master/pipeline.svg)](https://gitlab.com/mvysny/vaadin10-restdataprovider-example/commits/master)

# Vaadin 10 REST Data Provider Example App

Demonstrates the use of a full-blown REST DataProvider in a Vaadin 8 Grid. Please read more on
how this exactly works at [Accessing NoSQL or REST data sources](http://www.vaadinonkotlin.eu/nosql_rest_datasources.html).
To demo the REST capabilities totally standalone, this project:

* Configures Grid to fetch the data over REST from `localhost:8080/rest/articles`
* Exposes the REST CRUD endpoints on `localhost:8080/rest/articles` and uses vok-orm to fetch
data from an in-memory H2 database.

Only requires a Servlet 3.0 container to run. Developed in Kotlin. Also demoes an auto-generated
Grid filter bar, therefore this example serves as a full replacement for Teppo Kurki's
[FilteringTable](https://vaadin.com/directory/component/filteringtable).

The project uses code from [Vaadin-on-Kotlin](http://vaadinonkotlin.eu).

# Preparing Environment

The Vaadin 14 build requires `node.js` and `npm`. The Vaadin Gradle Plugin
is able to install those for you automatically (handy for the CI),
or you can install it to your OS:

* Windows: [node.js Download site](https://nodejs.org/en/download/) - use the .msi 64-bit installer
* Linux: `sudo apt install npm`

Also make sure that you have Java 8 (or higher) JDK installed.

# Getting Started

To quickly start the app, just type this into your terminal:

```bash
git clone https://gitlab.com/mvysny/vaadin10-restdataprovider-example
cd vaadin10-restdataprovider-example
./gradlew build web:appRun
```

Gradle will automatically download an embedded servlet container (Jetty) and will run your app in it. Your app will be running on
[http://localhost:8080](http://localhost:8080).

Since the build system is a Gradle file written in Kotlin, we suggest you
use [Intellij IDEA](https://www.jetbrains.com/idea/download)
to edit the project files. The Community edition is enough to run the server
via Gretty's `./gradlew appRun`. The Ultimate edition will allow you to run the
project in Tomcat - this is the recommended
option for a real development.

You can access the CRUD endpoint via
the command line:

```bash
curl "localhost:8080/rest/articles?sort_by=created,-title&score=4&title=ilike:the&offset=5"
```

or via the browser: [localhost:8080/rest/articles?sort_by=created,-title&score=4&title=ilike:the&offset=5](http://localhost:8080/rest/articles?sort_by=created,-title&score=4&title=ilike:the&offset=5)

When playing with the endpoint, do note that there are 1000 rows generated randomly but
the CRUD endpoint is limited to return at most 100 (this is of course configurable).

> Note: The CRUD endpoint support full filtering, sorting and paging capabilities.
For details please see the [vok-rest](https://github.com/mvysny/vaadin-on-kotlin/tree/master/vok-rest)
VoK module documentation.

Since the build system is a Gradle file written in Kotlin, we suggest you use [Intellij IDEA](https://www.jetbrains.com/idea/download)
to edit the project files. The Community edition is enough to run the server
via Gretty's `./gradlew web:appRun`. The Ultimate edition will allow you to run the project in Tomcat - this is the recommended
option for a real development.

## Supported Modes

Runs in Vaadin 14 npm mode, using the [Vaadin Gradle Plugin](https://github.com/vaadin/vaadin-gradle-plugin).

Both the [development and production modes](https://vaadin.com/docs/v14/flow/production/tutorial-production-mode-basic.html) are supported.
To prepare for development mode, just run:

```bash
./gradlew clean vaadinPrepareFrontend
```

To build in production mode, just run:

```bash
./gradlew clean build -Pvaadin.productionMode
```

If you don't have node installed in your CI environment, Vaadin plugin will install it for you automatically:

```bash
./gradlew clean build -Pvaadin.productionMode
```

# Workflow

To compile the entire project in production mode, run `./gradlew -Pvaadin.productionMode`.

To run the application in development mode, run `./gradlew appRun` and open [http://localhost:8080/](http://localhost:8080/).

To produce a deployable production-mode WAR:
- run `./gradlew -Pvaadin.productionMode`
- You will find the WAR file in `build/libs/*.war`
- To revert your environment back to development mode, just run `./gradlew` or `./gradlew vaadinPrepareFrontend`
  (omit the `-Pvaadin.productionMode`) switch.

This will allow you to quickly start the example app and allow you to do some basic modifications.
